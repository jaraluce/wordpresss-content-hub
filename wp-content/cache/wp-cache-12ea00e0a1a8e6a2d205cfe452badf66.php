<?php die(); ?>
<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="profile" href="http://gmpg.org/xfn/11">
      <link rel="pingback" href="">

      <title>HUB relevante.me</title>
<link rel="alternate" hreflang="en" href="http://hubdo.relevante.me" />
<link rel="alternate" hreflang="es" href="http://hubdo.relevante.me/?lang=es" />
<link rel='dns-prefetch' href='//vjs.zencdn.net' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="HUB relevante.me &raquo; Feed" href="http://hubdo.relevante.me/feed/" />
<link rel="alternate" type="application/rss+xml" title="HUB relevante.me &raquo; Comments Feed" href="http://hubdo.relevante.me/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/hubdo.relevante.me\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.1"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
				<style>
		.widget-customizer-highlighted-widget {
			outline: none;
			-webkit-box-shadow: 0 0 2px rgba(30,140,190,0.8);
			box-shadow: 0 0 2px rgba(30,140,190,0.8);
			position: relative;
			z-index: 1;
		}
		</style>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='validate-engine-css-css'  href='http://hubdo.relevante.me/wp-content/plugins/wysija-newsletters/css/validationEngine.jquery.css?ver=2.7.6' type='text/css' media='all' />
<link rel='stylesheet' id='videojs-plugin-css'  href='http://hubdo.relevante.me/wp-content/plugins/videojs-html5-video-player-for-wordpress/plugin-styles.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='videojs-css'  href='//vjs.zencdn.net/4.5/video-js.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='front-css-yuzo_related_post-css'  href='http://hubdo.relevante.me/wp-content/plugins/yuzo-related-post/assets/css/style.css?ver=5.12.68' type='text/css' media='all' />
<link rel='stylesheet' id='inbound-shortcodes-css'  href='http://hubdo.relevante.me/wp-content/plugins/cta/shared/shortcodes/css/frontend-render.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='wpml-legacy-horizontal-list-0-css'  href='http://hubdo.relevante.me/wp-content/plugins/sitepress-multilingual-cms/templates/language-switchers/legacy-list-horizontal/style.css?ver=1' type='text/css' media='all' />
<link rel='stylesheet' id='wpml-cms-nav-css-css'  href='http://hubdo.relevante.me/wp-content/plugins/wpml-cms-nav/res/css/navigation.css?ver=1.4.19' type='text/css' media='all' />
<link rel='stylesheet' id='cms-navigation-style-base-css'  href='http://hubdo.relevante.me/wp-content/plugins/wpml-cms-nav/res/css/cms-navigation-base.css?ver=1.4.19' type='text/css' media='screen' />
<link rel='stylesheet' id='cms-navigation-style-css'  href='http://hubdo.relevante.me/wp-content/plugins/wpml-cms-nav/res/css/cms-navigation.css?ver=1.4.19' type='text/css' media='screen' />
<link rel='stylesheet' id='masonic-style-css'  href='http://hubdo.relevante.me/wp-content/themes/masonic/style.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='masonic-google-fonts-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A400%2C300italic%2C700&#038;ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='masonic-font-awesome-css'  href='http://hubdo.relevante.me/wp-content/themes/masonic/font-awesome/css/font-awesome.min.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='http://hubdo.relevante.me/wp-includes/css/dashicons.min.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='customize-preview-css'  href='http://hubdo.relevante.me/wp-includes/css/customize-preview.min.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='__EPYT__style-css'  href='http://hubdo.relevante.me/wp-content/plugins/youtube-embed-plus/styles/ytprefs.min.css?ver=4.7.1' type='text/css' media='all' />
<style id='__EPYT__style-inline-css' type='text/css'>

                .epyt-gallery-thumb {
                        width: 33.333%;
                }
                
</style>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script><script>jQueryWP = jQuery;</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<!--[if lt IE 8]>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/json2.min.js?ver=2015-05-03'></script>
<![endif]-->
<!--[if lte IE 8]>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/html5shiv.js?ver=3.7.3'></script>
<![endif]-->
<script type='text/javascript'>
/* <![CDATA[ */
var cta_variation = {"cta_id":null,"admin_url":"http:\/\/hubdo.relevante.me\/wp-admin\/admin-ajax.php","home_url":"http:\/\/hubdo.relevante.me","disable_ajax":"0"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/cta/assets/js/cta-variation.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _EPYT_ = {"ajaxurl":"http:\/\/hubdo.relevante.me\/wp-admin\/admin-ajax.php","security":"74742debb9","gallery_scrolloffset":"20","eppathtoscripts":"http:\/\/hubdo.relevante.me\/wp-content\/plugins\/youtube-embed-plus\/scripts\/","epresponsiveselector":"[\"iframe.__youtube_prefs_widget__\"]","epdovol":"1","version":"11.5","evselector":"iframe.__youtube_prefs__[src], iframe[src*=\"youtube.com\/embed\/\"], iframe[src*=\"youtube-nocookie.com\/embed\/\"]"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/youtube-embed-plus/scripts/ytprefs.min.js?ver=4.7.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var inbound_settings = {"post_id":"46","variation_id":"0","ip_address":"83.59.99.163","wp_lead_data":{"lead_id":null,"lead_email":null,"lead_uid":"E9FuKcVuRTMuwK1eDefcnrvxqQ5gQysaykJ"},"admin_url":"http:\/\/hubdo.relevante.me\/wp-admin\/admin-ajax.php","track_time":"2017\/01\/23 18:47:36","post_type":"post","page_tracking":"on","search_tracking":"on","comment_tracking":"on","custom_mapping":[],"inbound_track_exclude":"","inbound_track_include":"","is_admin":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/cta/shared/assets/js/frontend/analytics/inboundAnalytics.min.js'></script>
<link rel='https://api.w.org/' href='http://hubdo.relevante.me/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://hubdo.relevante.me/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://hubdo.relevante.me/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.7.1" />
<link type="text/css" rel="stylesheet" href="http://hubdo.relevante.me/wp-content/plugins/category-specific-rss-feed-menu/wp_cat_rss_style.css" />
<style type='text/css'>#crestashareiconincontent {float: left;}#crestashareiconincontent {
   margin-top: 30px;
}</style><meta name="generator" content="WPML ver:3.6.2 stt:1,2;" />

		<script type="text/javascript"> document.createElement("video");document.createElement("audio");document.createElement("track"); </script>
		    <script>
    function hasWKGoogleAnalyticsCookie() {
      return (new RegExp('wp_wk_ga_untrack_' + document.location.hostname) ).test(document.cookie);
    }
    </script>
       <style type="text/css">
	     blockquote { border-left: 2px solid #2cc9ad; }
           .post-header .entry-author, .post-header .entry-standard, .post-header .entry-date, .post-header .entry-tag { color: #2cc9ad; }
           .entry-author, .entry-standard, .entry-date { color: #2cc9ad; }
           a:hover { color: #2cc9ad; }
           .widget_recent_entries li:before, .widget_recent_comments li:before { color: #2cc9ad; }
           .underline { background: none repeat scroll 0 0 #2cc9ad; }
           .widget-title { border-left: 3px solid #2cc9ad; }
           .sticky { border: 1px solid #2cc9ad; }
           .footer-background { border-top: 5px solid #2cc9ad; }
           .site-title a:hover { color: #2cc9ad; }
           button, input[type="button"], input[type="reset"], input[type="submit"] { background: none repeat scroll 0 0 #2cc9ad; }
           .breadcrums span { color: #2cc9ad; }
           .button:hover { color: #2cc9ad; }
           .catagory-type a:hover { color: #2cc9ad; }
           .copyright a span { color: #2cc9ad; }
           button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover { color: #2cc9ad; }
           .widget_rss li a:hover { color: #2cc9ad; }
           @media screen and (max-width: 768px) { nav li:hover ul li a:hover, nav li a:hover { background: #2cc9ad; } }
           .entry-date a .entry-date:hover { color: #2cc9ad; }
           .wp-pagenavi a, .wp-pagenavi span { border: 1px solid #2cc9ad; }
           </style>
   		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://hubdo.relevante.me/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://hubdo.relevante.me/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><style type="text/css" id="custom-background-css"></style><meta name='robots' content='noindex,follow' />
<style>
			body.wp-customizer-unloading {
				opacity: 0.25;
				cursor: progress !important;
				-webkit-transition: opacity 0.5s;
				transition: opacity 0.5s;
			}
			body.wp-customizer-unloading * {
				pointer-events: none !important;
			}
			form.customize-unpreviewable,
			form.customize-unpreviewable input,
			form.customize-unpreviewable select,
			form.customize-unpreviewable button,
			a.customize-unpreviewable,
			area.customize-unpreviewable {
				cursor: not-allowed !important;
			}
		</style>		<script>
		( function() {
			var urlParser, oldQueryParams, newQueryParams, i;
			if ( parent !== window ) {
				return;
			}
			urlParser = document.createElement( 'a' );
			urlParser.href = location.href;
			oldQueryParams = urlParser.search.substr( 1 ).split( /&/ );
			newQueryParams = [];
			for ( i = 0; i < oldQueryParams.length; i += 1 ) {
				if ( ! /^customize_messenger_channel=/.test( oldQueryParams[ i ] ) ) {
					newQueryParams.push( oldQueryParams[ i ] );
				}
			}
			urlParser.search = newQueryParams.join( '&' );
			if ( urlParser.search !== location.search ) {
				location.replace( urlParser.href );
			}
		} )();
		</script>
		<link rel="icon" href="/favicon.ico" sizes="32x32" />
		<style type="text/css" id="wp-custom-css">
			.menu-item-language img,
.menu-item-language ul li img {
    display: inline-table;
}

li.current-menu-item,
li.current-menu-item a,
li.current-category-ancestor,
li.current-post-ancestor {
    background: #2CC9AD !important;
}

.vc_btn3.vc_btn3-color-juicy-pink,
.vc_btn3.vc_btn3-color-juicy-pink {
    background: #2CC9AD !important;
}

.submenu {
    font-size: 0.8em;
}

.submenu li {
    width: 16%
}

.submenu li a {
    line-height: 1.4em;
}

.secondary {
    float: right;
}

.masonic-search input:active,
.masonic-search input:focus {
    color: white !important;
}



.entry-info .entry-date {
   display: block;
}

.entry-date a {
   font-size: 0.8em;
   vertical-align: baseline
}

.entry-date.fa-comments a {
   margin: 5px 10px 5px 0;
   text-transform: lowercase;
}

a:hover, a:focus, a:active, a.button {
   color: #2CC9AD;
}

.g-recaptcha {
   margin-bottom: 24px;
}

.widget_wysija_cont {
   padding: 0 25px;
}		</style>
	<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>   </head>

   <body class="home blog logged-in wpb-js-composer js-comp-ver-4.12 vc_responsive">
      <div id="page" class="hfeed site">
         <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

         <header id="masthead" class="site-header clear">

            <div class="header-image">
                                 <figure><img src="http://hubdo.relevante.me/wp-content/uploads/2016/08/cropped-cropped-firstTimeUser_bg.jpg" width="1350" height="500" alt="">
                     <div class="angled-background"></div>
                  </figure>
                           </div> <!-- .header-image -->

            <div class="site-branding clear">
               <div class="wrapper site-header-text clear">
                  
                     <div class="logo-img-holder " >
                        
                                          </div>
                  
                  <div class="main-header">
                                       <h1 class="site-title"><a href="http://hubdo.relevante.me/" rel="home">HUB relevante.me</a></h1>
                                                         <p class="site-description"></p>
                                    </div>
               </div>
            </div><!-- .site-branding -->

            <nav class="navigation clear">
               <input type="checkbox" id="masonic-toggle" name="masonic-toggle"/>
               <label for="masonic-toggle" id="masonic-toggle-label" class="fa fa-navicon fa-2x"></label>
               <div class="wrapper clear" id="masonic">
                  <ul  data-customize-partial-id="nav_menu_instance[b6b2d6e2e7aa4be6a6e1abf46d0bd604]" data-customize-partial-type="nav_menu_instance" data-customize-partial-placement-context="{&quot;after&quot;:&quot;&quot;,&quot;before&quot;:&quot;&quot;,&quot;can_partial_refresh&quot;:true,&quot;container&quot;:&quot;&quot;,&quot;container_class&quot;:&quot;&quot;,&quot;container_id&quot;:&quot;&quot;,&quot;depth&quot;:0,&quot;echo&quot;:true,&quot;fallback_cb&quot;:&quot;wp_page_menu&quot;,&quot;item_spacing&quot;:&quot;preserve&quot;,&quot;items_wrap&quot;:&quot;&lt;ul id=\&quot;%1$s\&quot; class=\&quot;%2$s wrapper clear\&quot;&gt;%3$s&lt;\/ul&gt;&quot;,&quot;link_after&quot;:&quot;&quot;,&quot;link_before&quot;:&quot;&quot;,&quot;menu&quot;:6,&quot;menu_class&quot;:&quot;menu&quot;,&quot;menu_id&quot;:&quot;&quot;,&quot;theme_location&quot;:&quot;primary&quot;,&quot;walker&quot;:&quot;&quot;,&quot;args_hmac&quot;:&quot;b6b2d6e2e7aa4be6a6e1abf46d0bd604&quot;}" id="menu-main-menu" class="menu wrapper clear"><li id="menu-item-52" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-52"><a href="/">Home</a></li>
<li id="menu-item-50" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-50"><a href="http://hubdo.relevante.me/category/marketing/">Marketing</a></li>
<li id="menu-item-51" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-51"><a href="http://hubdo.relevante.me/category/b2b/">B2B</a></li>
</ul>                                       <div id="sb-search" class="sb-search">
                        <span class="sb-icon-search"><i class="fa fa-search"></i></span>
                     </div>
                                 </div>
                                 <div id="sb-search-res" class="sb-search-res">
                     <span class="sb-icon-search"><i class="fa fa-search"></i></span>
                  </div>
                           </nav><!-- #site-navigation -->

            <div class="inner-wrap masonic-search-toggle">
               
<form role="search" method="get" class="searchform clear" action="http://hubdo.relevante.me/">
   <div class="masonic-search">
      <label class="screen-reader-text">Search for:</label>
      <input type="text" value="" name="s" placeholder="Type and hit enter..." />
   </div>
<input type='hidden' name='lang' value='en' /></form>            </div>

                     </header><!-- #masthead -->
<div class="site-content">
   <div id="container" class="wrapper clear">

      
                  
            
<article id="post-46" class="post-container post-46 post type-post status-publish format-standard has-post-thumbnail hentry category-b2b">
   <h2 class="entry-title"><a href="https://hub.uberflip.com/blog/how-b2b-brands-are-using-content-hubs" rel="bookmark">How B2B Brands Are Using Content Hubs</a></h2>
         <div class="wider-web-top">
         <i class="fa fa-2x fa-caret-down"></i>
      </div>
      <figure>
         <a href="https://hub.uberflip.com/blog/how-b2b-brands-are-using-content-hubs">
            <img width="350" height="205" src="http://hubdo.relevante.me/wp-content/uploads/2017/01/itemeditorimage_568c25cb8edbf-350x205.png" class="attachment-small-thumb size-small-thumb wp-post-image" alt="" />         </a>
      </figure>
   
         <div class="entry-info">
         <div class="entry-date fa fa-clock-o"><a href="https://hub.uberflip.com/blog/how-b2b-brands-are-using-content-hubs" title="5:10 pm" rel="bookmark"><time class="entry-date published" datetime="2017-01-23T17:10:56+00:00">January 23, 2017</time><time class="updated" datetime="2017-01-23T17:16:33+00:00">January 23, 2017</time></a></div><div class="entry-author vcard author fa fa-user"><a class="url fn n" href="http://hubdo.relevante.me/author/relevante-me/">relevante.me</a></div><div class="entry-standard fa fa-folder-open"><a href="http://hubdo.relevante.me/category/b2b/" rel="category tag">B2B</a></div>      </div><!-- .entry-meta -->
   
   <div class="entry-content">
      <p>As a marketer, it’s always easy to tell when a new year rolls around. If you stop, look, and listen, chances are you’ll notice a sudden and dramatic uptick in the words “…what to to expect in…” streaming across your</p>

            <a class="button" href="https://hub.uberflip.com/blog/how-b2b-brands-are-using-content-hubs">Read more</a>
   </div><!-- .entry-content -->

</article><!-- #post-## -->
         
            
<article id="post-40" class="post-container post-40 post type-post status-publish format-standard has-post-thumbnail hentry category-marketing">
   <h2 class="entry-title"><a href="https://www.linkedin.com/pulse/linkedin-strategy-selling-high-value-products-lawrence-klamecki?trk=prof-post" rel="bookmark">LinkedIn Strategy: Selling High Value Products and Services</a></h2>
         <div class="wider-web-top">
         <i class="fa fa-2x fa-caret-down"></i>
      </div>
      <figure>
         <a href="https://www.linkedin.com/pulse/linkedin-strategy-selling-high-value-products-lawrence-klamecki?trk=prof-post">
            <img width="350" height="205" src="http://hubdo.relevante.me/wp-content/uploads/2017/01/AAEAAQAAAAAAAAdUAAAAJDA4OTA5MWUzLTBlOGMtNGJmYi1iMDY2LWE2ZmI1NWI0YmVlNQ-350x205.jpg" class="attachment-small-thumb size-small-thumb wp-post-image" alt="" />         </a>
      </figure>
   
         <div class="entry-info">
         <div class="entry-date fa fa-clock-o"><a href="https://www.linkedin.com/pulse/linkedin-strategy-selling-high-value-products-lawrence-klamecki?trk=prof-post" title="5:09 pm" rel="bookmark"><time class="entry-date published" datetime="2017-01-23T17:09:29+00:00">January 23, 2017</time><time class="updated" datetime="2017-01-23T17:16:50+00:00">January 23, 2017</time></a></div><div class="entry-author vcard author fa fa-user"><a class="url fn n" href="http://hubdo.relevante.me/author/relevante-me/">relevante.me</a></div><div class="entry-standard fa fa-folder-open"><a href="http://hubdo.relevante.me/category/marketing/" rel="category tag">Marketing</a></div>      </div><!-- .entry-meta -->
   
   <div class="entry-content">
      <p>As the world&#8217;s #1 B2B social media network, LinkedIn is the ideal resource for selling high value offerings priced at $25k and up. These high ticket, low volume sales depend on building targeted relationships. This article outlines a strategy we</p>

            <a class="button" href="https://www.linkedin.com/pulse/linkedin-strategy-selling-high-value-products-lawrence-klamecki?trk=prof-post">Read more</a>
   </div><!-- .entry-content -->

</article><!-- #post-## -->
         
            
<article id="post-43" class="post-container post-43 post type-post status-publish format-standard has-post-thumbnail hentry category-b2b">
   <h2 class="entry-title"><a href="http://www.b2bleadblog.com/2016/12/what-can-b2b-marketers-adopt-from-growth-hacking.html" rel="bookmark">What Can B2B Marketers Gain from Growth Hacking?</a></h2>
         <div class="wider-web-top">
         <i class="fa fa-2x fa-caret-down"></i>
      </div>
      <figure>
         <a href="http://www.b2bleadblog.com/2016/12/what-can-b2b-marketers-adopt-from-growth-hacking.html">
            <img width="350" height="205" src="http://hubdo.relevante.me/wp-content/uploads/2017/01/Growthhacking-350x205.png" class="attachment-small-thumb size-small-thumb wp-post-image" alt="" />         </a>
      </figure>
   
         <div class="entry-info">
         <div class="entry-date fa fa-clock-o"><a href="http://www.b2bleadblog.com/2016/12/what-can-b2b-marketers-adopt-from-growth-hacking.html" title="5:08 pm" rel="bookmark"><time class="entry-date published" datetime="2017-01-23T17:08:20+00:00">January 23, 2017</time><time class="updated" datetime="2017-01-23T17:11:38+00:00">January 23, 2017</time></a></div><div class="entry-author vcard author fa fa-user"><a class="url fn n" href="http://hubdo.relevante.me/author/relevante-me/">relevante.me</a></div><div class="entry-standard fa fa-folder-open"><a href="http://hubdo.relevante.me/category/b2b/" rel="category tag">B2B</a></div>      </div><!-- .entry-meta -->
   
   <div class="entry-content">
      <p>What can you gain from growth hacking and how can you develop a mindset to be better at B2B marketing? To help answer this question, I interviewed Neil Patel (@neilpatel), co-founder of Crazy Egg, Hello Bar, and KISSmetrics. He also</p>

            <a class="button" href="http://www.b2bleadblog.com/2016/12/what-can-b2b-marketers-adopt-from-growth-hacking.html">Read more</a>
   </div><!-- .entry-content -->

</article><!-- #post-## -->
         
            
<article id="post-37" class="post-container post-37 post type-post status-publish format-standard has-post-thumbnail hentry category-marketing">
   <h2 class="entry-title"><a href="http://resources.pulsecomms.com/voodoo-account-based-marketing/" rel="bookmark">The voodoo of Account Based Marketing</a></h2>
         <div class="wider-web-top">
         <i class="fa fa-2x fa-caret-down"></i>
      </div>
      <figure>
         <a href="http://resources.pulsecomms.com/voodoo-account-based-marketing/">
            <img width="350" height="205" src="http://hubdo.relevante.me/wp-content/uploads/2017/01/Ricky-ABM-blog-696x500-350x205.png" class="attachment-small-thumb size-small-thumb wp-post-image" alt="" />         </a>
      </figure>
   
         <div class="entry-info">
         <div class="entry-date fa fa-clock-o"><a href="http://resources.pulsecomms.com/voodoo-account-based-marketing/" title="5:03 pm" rel="bookmark"><time class="entry-date published" datetime="2017-01-23T17:03:13+00:00">January 23, 2017</time><time class="updated" datetime="2017-01-23T17:13:22+00:00">January 23, 2017</time></a></div><div class="entry-author vcard author fa fa-user"><a class="url fn n" href="http://hubdo.relevante.me/author/relevante-me/">relevante.me</a></div><div class="entry-standard fa fa-folder-open"><a href="http://hubdo.relevante.me/category/marketing/" rel="category tag">Marketing</a></div>      </div><!-- .entry-meta -->
   
   <div class="entry-content">
      <p>Account based marketing is dividing marketers everywhere. It’s certainly a very fashionable term like ‘content marketing’ or ‘programmatic’ before it, but as always, it is very often misused. I’m sure there are marketers everywhere whose eyes roll when they hear</p>

            <a class="button" href="http://resources.pulsecomms.com/voodoo-account-based-marketing/">Read more</a>
   </div><!-- .entry-content -->

</article><!-- #post-## -->
         
      
   </div><!-- #container -->
   <div class="wrapper">
         </div>
</div><!-- #site-content -->


</div><!-- #page -->
<footer class="footer-background">
   <div class="footer-content wrapper clear">
      <div class="clear">
                     <div class="tg-one-third">
               		<aside id="recent-posts-4" class="widget widget_recent_entries">		<div class="widget-title"><h3>Recent posts</h3></div>		<ul>
					<li>
				<a href="https://hub.uberflip.com/blog/how-b2b-brands-are-using-content-hubs">How B2B Brands Are Using Content Hubs</a>
							<span class="post-date">January 23, 2017</span>
						</li>
					<li>
				<a href="https://www.linkedin.com/pulse/linkedin-strategy-selling-high-value-products-lawrence-klamecki?trk=prof-post">LinkedIn Strategy: Selling High Value Products and Services</a>
							<span class="post-date">January 23, 2017</span>
						</li>
					<li>
				<a href="http://www.b2bleadblog.com/2016/12/what-can-b2b-marketers-adopt-from-growth-hacking.html">What Can B2B Marketers Gain from Growth Hacking?</a>
							<span class="post-date">January 23, 2017</span>
						</li>
					<li>
				<a href="http://resources.pulsecomms.com/voodoo-account-based-marketing/">The voodoo of Account Based Marketing</a>
							<span class="post-date">January 23, 2017</span>
						</li>
				</ul>
		</aside>		            </div>
                              <div class="tg-one-third">
               <aside id="categories-3" class="widget widget_categories"><div class="widget-title"><h3>Categories</h3></div>		<ul>
	<li class="cat-item cat-item-4"><a href="http://hubdo.relevante.me/category/b2b/" >B2B</a> (2)
</li>
	<li class="cat-item cat-item-5"><a href="http://hubdo.relevante.me/category/marketing/" >Marketing</a> (2)
</li>
		</ul>
</aside>            </div>
                              <div class="tg-one-third last">
               <aside id="wysija-2" class="widget widget_wysija"><div class="widget-title"><h3>Get free updates from this HUB</h3></div><div class="widget_wysija_cont"><div id="msg-form-wysija-2" class="wysija-msg ajax"></div><form id="form-wysija-2" method="post" action="#wysija" class="widget_wysija">
<p class="wysija-paragraph">
    
    
    	<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="e-mail" placeholder="e-mail" value="" />
    
    
    
    <span class="abs-req">
        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
    </span>
    
</p>
<input class="wysija-submit wysija-submit-field" type="submit" value="Subscribe!" />

    <input type="hidden" name="form_id" value="1" />
    <input type="hidden" name="action" value="save" />
    <input type="hidden" name="controller" value="subscribers" />
    <input type="hidden" value="1" name="wysija-page" />

    
        <input type="hidden" name="wysija[user_list][list_ids]" value="1" />
    
 </form></div></aside>            </div>
               </div>
      <div class="copyright clear">
         <div class="copyright-header">HUB relevante.me</div>
         <div class="copyright-year">&copy; 2017</div>
         Powered by <a href="http://wordpress.org" target="_blank" title="WordPress"><span>WordPress</span></a> <br> Theme: Masonic by <a href="http://themegrill.com/themes/masonic" target="_blank" title="ThemeGrill" rel="author"><span>ThemeGrill</span></a>      </div>
   </div>
   <div class="angled-background"></div>
</footer>

<script>var _wpCustomizePreviewNavMenusExports = {"navMenuInstanceArgs":{"b6b2d6e2e7aa4be6a6e1abf46d0bd604":{"after":"","before":"","can_partial_refresh":true,"container":"","container_class":"","container_id":"","depth":0,"echo":true,"fallback_cb":"wp_page_menu","item_spacing":"preserve","items_wrap":"<ul id=\"%1$s\" class=\"%2$s wrapper clear\">%3$s<\/ul>","link_after":"","link_before":"","menu":6,"menu_class":"menu","menu_id":"","theme_location":"primary","walker":"","args_hmac":"b6b2d6e2e7aa4be6a6e1abf46d0bd604"}}};</script><style scoped>.yuzo_related_post{}
.yuzo_related_post .relatedthumb{}</style><script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/cta/shared//shortcodes/js/spin.min.js'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/wp-a11y.min.js?ver=4.7.1'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/customize-base.min.js?ver=4.7.1'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/customize-preview.min.js?ver=4.7.1'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/customizer.js?ver=20130508'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/yuzo-related-post/assets/js/jquery.equalizer.js?ver=5.12.68'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/imagesloaded.min.js?ver=3.2.0'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/masonry.min.js?ver=3.3.2'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/jquery/jquery.masonry.min.js?ver=3.1.2b'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/masonry-setting.js?ver=20150106'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/search-toggle.js?ver=20150106'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/fitvids/jquery.fitvids.js?ver=20150331'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/fitvids/fitvids-setting.js?ver=20150331'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/skip-link-focus-fix.js?ver=20130115'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/jquery.bxslider/jquery.bxslider.min.js?ver=20130115'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpUtilSettings = {"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
/* ]]> */
</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/wp-util.min.js?ver=4.7.1'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/customize-selective-refresh.min.js?ver=4.7.1'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/customize-preview-widgets.min.js?ver=4.7.1'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/customize-preview-nav-menus.min.js?ver=4.7.1'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/youtube-embed-plus/scripts/fitvids.min.js?ver=4.7.1'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/wp-embed.min.js?ver=4.7.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var icl_vars = {"current_language":"en","icl_home":"http:\/\/hubdo.relevante.me","ajax_url":"http:\/\/hubdo.relevante.me\/wp-admin\/admin-ajax.php","url_type":"3"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/sitepress-multilingual-cms/res/js/sitepress.js?ver=4.7.1'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-en.js?ver=2.7.6'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.6'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http:\/\/hubdo.relevante.me\/wp-admin\/admin-ajax.php","loadingTrans":"Loading...","is_rtl":""};
/* ]]> */
</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.6'></script>
		<script type="text/javascript">
			var _wpCustomizeSettings = {"changeset":{"uuid":"c128cf4f-d99d-4a9f-bb89-686c2cae3e2b"},"timeouts":{"selectiveRefresh":250,"keepAliveSend":1000},"theme":{"stylesheet":"masonic","active":true},"url":{"self":"\/","allowed":["http:\/\/hubdo.relevante.me\/"],"allowedHosts":["hubdo.relevante.me"],"isCrossDomain":false},"channel":"preview-0","activePanels":{"nav_menus":true,"widgets":true},"activeSections":{"menu_locations":true,"nav_menu[6]":true,"nav_menu[2]":true,"add_menu":true,"themes":true,"masonic_logo_section":true,"masonic_search_icon_display_section":true,"title_tagline":true,"colors":true,"header_image":true,"background_image":true,"masonic_featured_image_cropping_section":true,"static_front_page":true,"custom_css":true,"sidebar-widgets-right-sidebar":false,"sidebar-widgets-footer-sidebar-one":true,"sidebar-widgets-footer-sidebar-two":true,"sidebar-widgets-footer-sidebar-three":true},"activeControls":{"custom_logo":true,"theme_flymag":true,"theme_twentyfifteen":true,"theme_twentyfourteen":true,"theme_twentysixteen":true,"blogname":true,"blogdescription":true,"header_textcolor":true,"background_color":true,"header_image":true,"background_image":true,"background_preset":true,"background_position":true,"background_size":true,"background_repeat":true,"background_attachment":true,"show_on_front":true,"page_on_front":true,"page_for_posts":true,"custom_css":true,"masonic_search_icon_display":true,"masonic_featured_image_cropping":true,"nav_menu_locations[primary]":true,"nav_menu_item[52]":true,"nav_menu_item[20]":true,"new_menu_name":true,"create_new_menu":true,"nav_menu_item[50]":true,"nav_menu_item[51]":true,"masonic_primary_color":true,"masonic_link_color":true,"display_header_text":true,"site_icon":true,"sidebars_widgets[right-sidebar]":true,"widget_id_wp_cta_static_widget[2]":true,"sidebars_widgets[footer-sidebar-one]":true,"widget_recent-posts[4]":true,"sidebars_widgets[footer-sidebar-two]":true,"widget_categories[3]":true,"sidebars_widgets[footer-sidebar-three]":true,"widget_wysija[2]":true},"settingValidities":[],"nonce":{"save":"7c3c52f9c3","preview":"f107a71df2","update-widget":"6a8d762022","customize-menus":"116cd21b8d"},"l10n":{"shiftClickToEdit":"Shift-click to edit this element.","linkUnpreviewable":"This link is not live-previewable.","formUnpreviewable":"This form is not live-previewable."},"_dirty":[]};
			_wpCustomizeSettings.values = {};
			(function( v ) {
				v["active_theme"] = "";
v["blogname"] = "HUB relevante.me";
v["blogdescription"] = "";
v["site_icon"] = "0";
v["custom_logo"] = "";
v["header_textcolor"] = "#FFFFFF";
v["background_color"] = "#ffffff";
v["header_image"] = "http:\/\/hubdo.relevante.me\/wp-content\/uploads\/2016\/08\/cropped-cropped-firstTimeUser_bg.jpg";
v["header_image_data"] = {"attachment_id":31,"url":"http:\/\/hubdo.relevante.me\/wp-content\/uploads\/2016\/08\/cropped-cropped-firstTimeUser_bg.jpg","thumbnail_url":"http:\/\/hubdo.relevante.me\/wp-content\/uploads\/2016\/08\/cropped-cropped-firstTimeUser_bg.jpg","height":500,"width":1350};
v["background_image"] = "";
v["background_image_thumb"] = "";
v["background_preset"] = "default";
v["background_position_x"] = "left";
v["background_position_y"] = "top";
v["background_size"] = "auto";
v["background_repeat"] = "repeat";
v["background_attachment"] = "scroll";
v["show_on_front"] = "posts";
v["page_on_front"] = "0";
v["page_for_posts"] = "0";
v["custom_css[masonic]"] = ".menu-item-language img,\n.menu-item-language ul li img {\n    display: inline-table;\n}\n\nli.current-menu-item,\nli.current-menu-item a,\nli.current-category-ancestor,\nli.current-post-ancestor {\n    background: #2CC9AD !important;\n}\n\n.vc_btn3.vc_btn3-color-juicy-pink,\n.vc_btn3.vc_btn3-color-juicy-pink {\n    background: #2CC9AD !important;\n}\n\n.submenu {\n    font-size: 0.8em;\n}\n\n.submenu li {\n    width: 16%\n}\n\n.submenu li a {\n    line-height: 1.4em;\n}\n\n.secondary {\n    float: right;\n}\n\n.masonic-search input:active,\n.masonic-search input:focus {\n    color: white !important;\n}\n\n\n\n.entry-info .entry-date {\n   display: block;\n}\n\n.entry-date a {\n   font-size: 0.8em;\n   vertical-align: baseline\n}\n\n.entry-date.fa-comments a {\n   margin: 5px 10px 5px 0;\n   text-transform: lowercase;\n}\n\na:hover, a:focus, a:active, a.button {\n   color: #2CC9AD;\n}\n\n.g-recaptcha {\n   margin-bottom: 24px;\n}\n\n.widget_wysija_cont {\n   padding: 0 25px;\n}";
v["masonic_primary_color"] = "#2cc9ad";
v["masonic_link_color"] = "#6a6a6a";
v["masonic_search_icon_display"] = 1;
v["masonic_featured_image_cropping"] = 1;
v["nav_menu_locations[primary]"] = 6;
v["nav_menu[6]"] = {"name":"Main menu","description":"","parent":0,"auto_add":false};
v["nav_menu_item[52]"] = {"menu_item_parent":0,"object_id":52,"object":"custom","type":"custom","type_label":"Custom Link","title":"Home","url":"\/","target":"","attr_title":"","description":"","classes":"","xfn":"","nav_menu_term_id":6,"position":1,"status":"publish","original_title":"","_invalid":false};
v["nav_menu_item[50]"] = {"menu_item_parent":0,"object_id":5,"object":"category","type":"taxonomy","type_label":"Category","url":"http:\/\/hubdo.relevante.me\/category\/marketing\/","title":"","target":"","attr_title":"","description":"","classes":"","xfn":"","nav_menu_term_id":6,"position":2,"status":"publish","original_title":"Marketing","_invalid":false};
v["nav_menu_item[51]"] = {"menu_item_parent":0,"object_id":4,"object":"category","type":"taxonomy","type_label":"Category","url":"http:\/\/hubdo.relevante.me\/category\/b2b\/","title":"","target":"","attr_title":"","description":"","classes":"","xfn":"","nav_menu_term_id":6,"position":3,"status":"publish","original_title":"B2B","_invalid":false};
v["nav_menu[2]"] = {"name":"Menu 1","description":"","parent":0,"auto_add":false};
v["nav_menu_item[20]"] = {"menu_item_parent":0,"object_id":20,"object":"custom","type":"custom","type_label":"Custom Link","title":"Home","url":"\/","target":"","attr_title":"","description":"","classes":"","xfn":"","nav_menu_term_id":2,"position":1,"status":"publish","original_title":"","_invalid":false};
v["nav_menus_created_posts"] = [];
v["widget_pages[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_calendar[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_archives[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_meta[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_search[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_text[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_categories[3]"] = {"encoded_serialized_instance":"YTo0OntzOjU6InRpdGxlIjtzOjEwOiJDYXRlZ29yaWVzIjtzOjU6ImNvdW50IjtpOjE7czoxMjoiaGllcmFyY2hpY2FsIjtpOjA7czo4OiJkcm9wZG93biI7aTowO30=","title":"Categories","is_widget_customizer_js_value":true,"instance_hash_key":"d7f534f7edff655c6c5de2346dd72eb2"};
v["widget_recent-posts[4]"] = {"encoded_serialized_instance":"YTozOntzOjU6InRpdGxlIjtzOjEyOiJSZWNlbnQgcG9zdHMiO3M6NjoibnVtYmVyIjtpOjU7czo5OiJzaG93X2RhdGUiO2I6MTt9","title":"Recent posts","is_widget_customizer_js_value":true,"instance_hash_key":"ff3a87c2335ed241b92b1b061ca57033"};
v["widget_recent-comments[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_rss[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_tag_cloud[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_nav_menu[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_wysija[2]"] = {"encoded_serialized_instance":"YToyOntzOjU6InRpdGxlIjtzOjMwOiJHZXQgZnJlZSB1cGRhdGVzIGZyb20gdGhpcyBIVUIiO3M6NDoiZm9ybSI7czoxOiIxIjt9","title":"Get free updates from this HUB","is_widget_customizer_js_value":true,"instance_hash_key":"2f548c0333ee2e8e926c81de91f038e7"};
v["widget_id_wp_cta_static_widget[2]"] = {"encoded_serialized_instance":"YTo1OntzOjE0OiJjdGFfbWFyZ2luX3RvcCI7czowOiIiO3M6MTc6ImN0YV9tYXJnaW5fYm90dG9tIjtzOjI6IjMwIjtzOjc6ImN0YV9pZHMiO2E6MTp7aTowO3M6MjoiMjYiO31zOjQ2OiJleHRlbmRlZF93aWRnZXRfb3B0cy1pZF93cF9jdGFfc3RhdGljX3dpZGdldC0yIjthOjU6e3M6MTA6InZpc2liaWxpdHkiO2E6Mjp7czo3OiJvcHRpb25zIjtzOjQ6ImhpZGUiO3M6ODoic2VsZWN0ZWQiO3M6MToiMCI7fXM6NzoiZGV2aWNlcyI7YToxOntzOjc6Im9wdGlvbnMiO3M6NDoiaGlkZSI7fXM6OToiYWxpZ25tZW50IjthOjE6e3M6NzoiZGVza3RvcCI7czo3OiJkZWZhdWx0Ijt9czo1OiJjbGFzcyI7YTo0OntzOjg6InNlbGVjdGVkIjtzOjE6IjAiO3M6MjoiaWQiO3M6MDoiIjtzOjc6ImNsYXNzZXMiO3M6MDoiIjtzOjU6ImxvZ2ljIjtzOjA6IiI7fXM6OToidGFic2VsZWN0IjtzOjE6IjAiO31zOjU6InRpdGxlIjtiOjA7fQ==","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"ba88279e4e8d803f01b9ec5d5909de92"};
v["widget_id_wp_cta_dynamic_widget[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_yuzo_widget[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_sidebar-navigation[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_text_icl[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_icl_lang_sel_widget[1]"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["widget_category_rss_widgets"] = {"encoded_serialized_instance":"YTowOnt9","title":"","is_widget_customizer_js_value":true,"instance_hash_key":"289fe60e4baf3963969c97b493b2b9e5"};
v["sidebars_widgets[wp_inactive_widgets]"] = [];
v["sidebars_widgets[right-sidebar]"] = ["id_wp_cta_static_widget-2"];
v["sidebars_widgets[footer-sidebar-one]"] = ["recent-posts-4"];
v["sidebars_widgets[footer-sidebar-two]"] = ["categories-3"];
v["sidebars_widgets[footer-sidebar-three]"] = ["wysija-2"];
			})( _wpCustomizeSettings.values );
		</script>
				<script type="text/javascript">
			var _wpWidgetCustomizerPreviewSettings = {"renderedSidebars":{"footer-sidebar-one":true,"footer-sidebar-two":true,"footer-sidebar-three":true},"renderedWidgets":{"recent-posts-4":true,"categories-3":true,"wysija-2":true},"registeredSidebars":[{"name":"Right Sidebar","id":"right-sidebar","description":"","class":"","before_widget":"<aside id=\"%1$s\" class=\"blog-post widget %2$s\">","after_widget":"<\/aside>","before_title":"<div class=\"widget-title\"><h3>","after_title":"<\/h3><\/div>"},{"name":"Footer Sidebar One","id":"footer-sidebar-one","description":"","class":"","before_widget":"<aside id=\"%1$s\" class=\"widget %2$s\">","after_widget":"<\/aside>","before_title":"<div class=\"widget-title\"><h3>","after_title":"<\/h3><\/div>"},{"name":"Footer Sidebar Two","id":"footer-sidebar-two","description":"","class":"","before_widget":"<aside id=\"%1$s\" class=\"widget %2$s\">","after_widget":"<\/aside>","before_title":"<div class=\"widget-title\"><h3>","after_title":"<\/h3><\/div>"},{"name":"Footer Sidebar Three","id":"footer-sidebar-three","description":"","class":"","before_widget":"<aside id=\"%1$s\" class=\"widget %2$s\">","after_widget":"<\/aside>","before_title":"<div class=\"widget-title\"><h3>","after_title":"<\/h3><\/div>"}],"registeredWidgets":{"pages-1":{"name":"Pages","id":"pages-1","params":[{"number":-1}],"classname":"widget_pages","customize_selective_refresh":true,"description":"A list of your site&#8217;s Pages."},"calendar-1":{"name":"Calendar","id":"calendar-1","params":[{"number":-1}],"classname":"widget_calendar","customize_selective_refresh":true,"description":"A calendar of your site&#8217;s Posts."},"archives-1":{"name":"Archives","id":"archives-1","params":[{"number":-1}],"classname":"widget_archive","customize_selective_refresh":true,"description":"A monthly archive of your site&#8217;s Posts."},"meta-1":{"name":"Meta","id":"meta-1","params":[{"number":-1}],"classname":"widget_meta","customize_selective_refresh":true,"description":"Login, RSS, &amp; WordPress.org links."},"search-1":{"name":"Search","id":"search-1","params":[{"number":-1}],"classname":"widget_search","customize_selective_refresh":true,"description":"A search form for your site."},"text-1":{"name":"Text","id":"text-1","params":[{"number":1}],"classname":"widget_text","customize_selective_refresh":true,"description":"Arbitrary text or HTML."},"categories-3":{"name":"Categories","id":"categories-3","params":[{"number":3}],"classname":"widget_categories","customize_selective_refresh":true,"description":"A list or dropdown of categories."},"recent-posts-4":{"name":"Recent Posts","id":"recent-posts-4","params":[{"number":4}],"classname":"widget_recent_entries","customize_selective_refresh":true,"description":"Your site&#8217;s most recent Posts."},"recent-comments-1":{"name":"Recent Comments","id":"recent-comments-1","params":[{"number":-1}],"classname":"widget_recent_comments","customize_selective_refresh":true,"description":"Your site&#8217;s most recent comments."},"rss-1":{"name":"RSS","id":"rss-1","params":[{"number":1}],"classname":"widget_rss","customize_selective_refresh":true,"description":"Entries from any RSS or Atom feed."},"tag_cloud-1":{"name":"Tag Cloud","id":"tag_cloud-1","params":[{"number":-1}],"classname":"widget_tag_cloud","customize_selective_refresh":true,"description":"A cloud of your most used tags."},"nav_menu-1":{"name":"Custom Menu","id":"nav_menu-1","params":[{"number":-1}],"classname":"widget_nav_menu","customize_selective_refresh":true,"description":"Add a custom menu to your sidebar."},"wysija-2":{"name":"MailPoet Subscription Form","id":"wysija-2","params":[{"number":2}],"classname":"widget_wysija","customize_selective_refresh":false,"description":"Subscription form for your newsletters."},"id_wp_cta_static_widget-2":{"name":"Call to Action Static Widget","id":"id_wp_cta_static_widget-2","params":[{"number":2}],"classname":"class_CTA_Static_Widget","customize_selective_refresh":false,"description":"Use this widget to manually display Calls to Action in sidebars."},"id_wp_cta_dynamic_widget-1":{"name":"Call to Action Placement Holder","id":"id_wp_cta_dynamic_widget-1","params":[{"number":-1}],"classname":"class_CTA_Dynamic_Widget","customize_selective_refresh":false,"description":"Use this widget to accept Calls to Action placements."},"yuzo_widget-1":{"name":"Yuzo","id":"yuzo_widget-1","params":[{"number":-1}],"classname":"yuzo_widget","customize_selective_refresh":false,"description":"Show related and more..."},"sidebar-navigation-1":{"name":"Sidebar Navigation","id":"sidebar-navigation-1","params":[{"number":-1}],"classname":"icl_sidebar_navigation","customize_selective_refresh":false,"description":"Sidebar Navigation"},"text_icl-1":{"name":"Multilingual Text","id":"text_icl-1","params":[{"number":-1}],"classname":"widget_text_icl","customize_selective_refresh":false,"description":"Multilingual arbitrary text or HTML"},"icl_lang_sel_widget-1":{"name":"Language Switcher","id":"icl_lang_sel_widget-1","params":[{"number":-1}],"classname":"widget_icl_lang_sel_widget","customize_selective_refresh":false,"description":"Language Switcher"},"category_rss_widgets":{"name":"Category Specific RSS","id":"category_rss_widgets","params":[],"classname":"widget_cat_spec_rss","description":"Display Category Specific RSS Menu."}},"l10n":{"widgetTooltip":"Shift-click to edit this widget."},"selectiveRefreshableWidgets":[]};
		</script>
		<script>var pltNewTabURLs = ["https:\/\/hub.uberflip.com\/blog\/how-b2b-brands-are-using-content-hubs","https:\/\/www.linkedin.com\/pulse\/linkedin-strategy-selling-high-value-products-lawrence-klamecki?trk=prof-post","http:\/\/www.b2bleadblog.com\/2016\/12\/what-can-b2b-marketers-adopt-from-growth-hacking.html","http:\/\/resources.pulsecomms.com\/voodoo-account-based-marketing\/"];(function(){(function(e){var t;t=e.jQueryWP||e.jQuery;return t(function(e){return typeof e.fn.on=="function"?e("body").on("click","a",function(t){var n;n=e(this);if(e.inArray(n.attr("href"),pltNewTabURLs)>-1)return n.attr("target","_blank")}):typeof console!="undefined"&&console!==null?console.log("Page Links To: Some other code has overridden the WordPress copy of jQuery. This is bad. Because of this, Page Links To cannot open links in a new window."):void 0})})(window)}).call(this);</script><script>var _customizePartialRefreshExports = {"partials":{"custom_logo":{"settings":["custom_logo"],"primarySetting":"custom_logo","selector":".custom-logo-link","type":"default","fallbackRefresh":true,"containerInclusive":true}},"renderQueryVar":"wp_customize_render_partials","l10n":{"shiftClickToEdit":"Shift-click to edit this element.","clickEditMenu":"Click to edit this menu.","clickEditWidget":"Click to edit this widget.","clickEditTitle":"Click to edit the site title.","clickEditMisc":"Click to edit this element.","badDocumentWrite":"document.write() is forbidden"}};</script>
</body>
</html>
<!-- Dynamic page generated in 0.807 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2017-01-23 18:47:36 -->
