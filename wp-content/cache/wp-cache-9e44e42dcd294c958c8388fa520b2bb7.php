<?php die(); ?>
<!DOCTYPE html>
<html lang="en-US">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="profile" href="http://gmpg.org/xfn/11">
      <link rel="pingback" href="">

      <title>Marketing &#8211; HUB relevante.me</title>
<link rel="alternate" hreflang="en" href="http://hubdo.relevante.me/category/marketing/" />
<link rel='dns-prefetch' href='//vjs.zencdn.net' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="HUB relevante.me &raquo; Feed" href="http://hubdo.relevante.me/feed/" />
<link rel="alternate" type="application/rss+xml" title="HUB relevante.me &raquo; Comments Feed" href="http://hubdo.relevante.me/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="HUB relevante.me &raquo; Marketing Category Feed" href="http://hubdo.relevante.me/category/marketing/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/hubdo.relevante.me\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.7.1"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='validate-engine-css-css'  href='http://hubdo.relevante.me/wp-content/plugins/wysija-newsletters/css/validationEngine.jquery.css?ver=2.7.6' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='http://hubdo.relevante.me/wp-includes/css/dashicons.min.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-css'  href='http://hubdo.relevante.me/wp-includes/css/admin-bar.min.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='videojs-plugin-css'  href='http://hubdo.relevante.me/wp-content/plugins/videojs-html5-video-player-for-wordpress/plugin-styles.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='videojs-css'  href='//vjs.zencdn.net/4.5/video-js.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='front-css-yuzo_related_post-css'  href='http://hubdo.relevante.me/wp-content/plugins/yuzo-related-post/assets/css/style.css?ver=5.12.68' type='text/css' media='all' />
<link rel='stylesheet' id='inbound-shortcodes-css'  href='http://hubdo.relevante.me/wp-content/plugins/cta/shared/shortcodes/css/frontend-render.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='wpml-legacy-horizontal-list-0-css'  href='http://hubdo.relevante.me/wp-content/plugins/sitepress-multilingual-cms/templates/language-switchers/legacy-list-horizontal/style.css?ver=1' type='text/css' media='all' />
<link rel='stylesheet' id='wpml-cms-nav-css-css'  href='http://hubdo.relevante.me/wp-content/plugins/wpml-cms-nav/res/css/navigation.css?ver=1.4.19' type='text/css' media='all' />
<link rel='stylesheet' id='cms-navigation-style-base-css'  href='http://hubdo.relevante.me/wp-content/plugins/wpml-cms-nav/res/css/cms-navigation-base.css?ver=1.4.19' type='text/css' media='screen' />
<link rel='stylesheet' id='cms-navigation-style-css'  href='http://hubdo.relevante.me/wp-content/plugins/wpml-cms-nav/res/css/cms-navigation.css?ver=1.4.19' type='text/css' media='screen' />
<link rel='stylesheet' id='masonic-style-css'  href='http://hubdo.relevante.me/wp-content/themes/masonic/style.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='masonic-google-fonts-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A400%2C300italic%2C700&#038;ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='masonic-font-awesome-css'  href='http://hubdo.relevante.me/wp-content/themes/masonic/font-awesome/css/font-awesome.min.css?ver=4.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='__EPYT__style-css'  href='http://hubdo.relevante.me/wp-content/plugins/youtube-embed-plus/styles/ytprefs.min.css?ver=4.7.1' type='text/css' media='all' />
<style id='__EPYT__style-inline-css' type='text/css'>

                .epyt-gallery-thumb {
                        width: 33.333%;
                }
                
</style>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script><script>jQueryWP = jQuery;</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<!--[if lte IE 8]>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/html5shiv.js?ver=3.7.3'></script>
<![endif]-->
<script type='text/javascript'>
/* <![CDATA[ */
var cta_variation = {"cta_id":null,"admin_url":"http:\/\/hubdo.relevante.me\/wp-admin\/admin-ajax.php","home_url":"http:\/\/hubdo.relevante.me","disable_ajax":"0"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/cta/assets/js/cta-variation.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _EPYT_ = {"ajaxurl":"http:\/\/hubdo.relevante.me\/wp-admin\/admin-ajax.php","security":"74742debb9","gallery_scrolloffset":"20","eppathtoscripts":"http:\/\/hubdo.relevante.me\/wp-content\/plugins\/youtube-embed-plus\/scripts\/","epresponsiveselector":"[\"iframe.__youtube_prefs_widget__\"]","epdovol":"1","version":"11.5","evselector":"iframe.__youtube_prefs__[src], iframe[src*=\"youtube.com\/embed\/\"], iframe[src*=\"youtube-nocookie.com\/embed\/\"]"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/youtube-embed-plus/scripts/ytprefs.min.js?ver=4.7.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var inbound_settings = {"post_id":"5","variation_id":"0","ip_address":"83.59.99.163","wp_lead_data":{"lead_id":null,"lead_email":null,"lead_uid":"E9FuKcVuRTMuwK1eDefcnrvxqQ5gQysaykJ"},"admin_url":"http:\/\/hubdo.relevante.me\/wp-admin\/admin-ajax.php","track_time":"2017\/01\/23 18:39:00","post_type":"post","page_tracking":"on","search_tracking":"on","comment_tracking":"on","custom_mapping":[],"inbound_track_exclude":"","inbound_track_include":"","is_admin":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/cta/shared/assets/js/frontend/analytics/inboundAnalytics.min.js'></script>
<link rel='https://api.w.org/' href='http://hubdo.relevante.me/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://hubdo.relevante.me/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://hubdo.relevante.me/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.7.1" />
<link type="text/css" rel="stylesheet" href="http://hubdo.relevante.me/wp-content/plugins/category-specific-rss-feed-menu/wp_cat_rss_style.css" />
<meta name="generator" content="WPML ver:3.6.2 stt:1,2;" />

		<script type="text/javascript"> document.createElement("video");document.createElement("audio");document.createElement("track"); </script>
		    <script>
    function hasWKGoogleAnalyticsCookie() {
      return (new RegExp('wp_wk_ga_untrack_' + document.location.hostname) ).test(document.cookie);
    }
    </script>
       <style type="text/css">
	     blockquote { border-left: 2px solid #2cc9ad; }
           .post-header .entry-author, .post-header .entry-standard, .post-header .entry-date, .post-header .entry-tag { color: #2cc9ad; }
           .entry-author, .entry-standard, .entry-date { color: #2cc9ad; }
           a:hover { color: #2cc9ad; }
           .widget_recent_entries li:before, .widget_recent_comments li:before { color: #2cc9ad; }
           .underline { background: none repeat scroll 0 0 #2cc9ad; }
           .widget-title { border-left: 3px solid #2cc9ad; }
           .sticky { border: 1px solid #2cc9ad; }
           .footer-background { border-top: 5px solid #2cc9ad; }
           .site-title a:hover { color: #2cc9ad; }
           button, input[type="button"], input[type="reset"], input[type="submit"] { background: none repeat scroll 0 0 #2cc9ad; }
           .breadcrums span { color: #2cc9ad; }
           .button:hover { color: #2cc9ad; }
           .catagory-type a:hover { color: #2cc9ad; }
           .copyright a span { color: #2cc9ad; }
           button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover { color: #2cc9ad; }
           .widget_rss li a:hover { color: #2cc9ad; }
           @media screen and (max-width: 768px) { nav li:hover ul li a:hover, nav li a:hover { background: #2cc9ad; } }
           .entry-date a .entry-date:hover { color: #2cc9ad; }
           .wp-pagenavi a, .wp-pagenavi span { border: 1px solid #2cc9ad; }
           </style>
   		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="http://hubdo.relevante.me/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="http://hubdo.relevante.me/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
		<style type="text/css" id="wp-custom-css">
			.menu-item-language img,
.menu-item-language ul li img {
    display: inline-table;
}

li.current-menu-item,
li.current-menu-item a,
li.current-category-ancestor,
li.current-post-ancestor {
    background: #2CC9AD !important;
}

.vc_btn3.vc_btn3-color-juicy-pink,
.vc_btn3.vc_btn3-color-juicy-pink {
    background: #2CC9AD !important;
}

.submenu {
    font-size: 0.8em;
}

.submenu li {
    width: 16%
}

.submenu li a {
    line-height: 1.4em;
}

.secondary {
    float: right;
}

.masonic-search input:active,
.masonic-search input:focus {
    color: white !important;
}



.entry-info .entry-date {
   display: block;
}

.entry-date a {
   font-size: 0.8em;
   vertical-align: baseline
}

.entry-date.fa-comments a {
   margin: 5px 10px 5px 0;
   text-transform: lowercase;
}

a:hover, a:focus, a:active, a.button {
   color: #2CC9AD;
}

.g-recaptcha {
   margin-bottom: 24px;
}

.widget_wysija_cont {
   padding: 0 25px;
}		</style>
	<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>   </head>

   <body class="archive category category-marketing category-5 logged-in admin-bar no-customize-support wpb-js-composer js-comp-ver-4.12 vc_responsive">
      <div id="page" class="hfeed site">
         <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

         <header id="masthead" class="site-header clear">

            <div class="header-image">
                                 <figure><img src="http://hubdo.relevante.me/wp-content/uploads/2016/08/cropped-cropped-firstTimeUser_bg.jpg" width="1350" height="500" alt="">
                     <div class="angled-background"></div>
                  </figure>
                           </div> <!-- .header-image -->

            <div class="site-branding clear">
               <div class="wrapper site-header-text clear">
                  
                     <div class="logo-img-holder " >
                        
                                          </div>
                  
                  <div class="main-header">
                                       <h3 class="site-title"><a href="http://hubdo.relevante.me/" rel="home">HUB relevante.me</a></h3>
                                                      </div>
               </div>
            </div><!-- .site-branding -->

            <nav class="navigation clear">
               <input type="checkbox" id="masonic-toggle" name="masonic-toggle"/>
               <label for="masonic-toggle" id="masonic-toggle-label" class="fa fa-navicon fa-2x"></label>
               <div class="wrapper clear" id="masonic">
                  <ul id="menu-main-menu" class="menu wrapper clear"><li id="menu-item-52" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-52"><a href="/">Home</a></li>
<li id="menu-item-50" class="menu-item menu-item-type-taxonomy menu-item-object-category current-menu-item menu-item-50"><a href="http://hubdo.relevante.me/category/marketing/">Marketing</a></li>
<li id="menu-item-51" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-51"><a href="http://hubdo.relevante.me/category/b2b/">B2B</a></li>
</ul>                                       <div id="sb-search" class="sb-search">
                        <span class="sb-icon-search"><i class="fa fa-search"></i></span>
                     </div>
                                 </div>
                                 <div id="sb-search-res" class="sb-search-res">
                     <span class="sb-icon-search"><i class="fa fa-search"></i></span>
                  </div>
                           </nav><!-- #site-navigation -->

            <div class="inner-wrap masonic-search-toggle">
               
<form role="search" method="get" class="searchform clear" action="http://hubdo.relevante.me/">
   <div class="masonic-search">
      <label class="screen-reader-text">Search for:</label>
      <input type="text" value="" name="s" placeholder="Type and hit enter..." />
   </div>
<input type='hidden' name='lang' value='en' /></form>            </div>

                           <div class="blog-header clear">
                  <article class="wrapper">
                     <div class="blog-site-title">
                                                                                 <h1>Category: Marketing</h1>
                                                                        </div>

                     
                  </article>
               </div>
                     </header><!-- #masthead -->
<div class="site-content">
   <div id="container" class="wrapper clear">

      
                  
            
<article id="post-40" class="post-container post-40 post type-post status-publish format-standard has-post-thumbnail hentry category-marketing">
   <h2 class="entry-title"><a href="https://www.linkedin.com/pulse/linkedin-strategy-selling-high-value-products-lawrence-klamecki?trk=prof-post" rel="bookmark">LinkedIn Strategy: Selling High Value Products and Services</a></h2>
         <div class="wider-web-top">
         <i class="fa fa-2x fa-caret-down"></i>
      </div>
      <figure>
         <a href="https://www.linkedin.com/pulse/linkedin-strategy-selling-high-value-products-lawrence-klamecki?trk=prof-post">
            <img width="350" height="205" src="http://hubdo.relevante.me/wp-content/uploads/2017/01/AAEAAQAAAAAAAAdUAAAAJDA4OTA5MWUzLTBlOGMtNGJmYi1iMDY2LWE2ZmI1NWI0YmVlNQ-350x205.jpg" class="attachment-small-thumb size-small-thumb wp-post-image" alt="" />         </a>
      </figure>
   
         <div class="entry-info">
         <div class="entry-date fa fa-clock-o"><a href="https://www.linkedin.com/pulse/linkedin-strategy-selling-high-value-products-lawrence-klamecki?trk=prof-post" title="5:09 pm" rel="bookmark"><time class="entry-date published" datetime="2017-01-23T17:09:29+00:00">January 23, 2017</time><time class="updated" datetime="2017-01-23T17:16:50+00:00">January 23, 2017</time></a></div><div class="entry-author vcard author fa fa-user"><a class="url fn n" href="http://hubdo.relevante.me/author/relevante-me/">relevante.me</a></div><div class="entry-standard fa fa-folder-open"><a href="http://hubdo.relevante.me/category/marketing/" rel="category tag">Marketing</a></div>      </div><!-- .entry-meta -->
   
   <div class="entry-content">
      <p>As the world&#8217;s #1 B2B social media network, LinkedIn is the ideal resource for selling high value offerings priced at $25k and up. These high ticket, low volume sales depend on building targeted relationships. This article outlines a strategy we</p>

            <a class="button" href="https://www.linkedin.com/pulse/linkedin-strategy-selling-high-value-products-lawrence-klamecki?trk=prof-post">Read more</a>
   </div><!-- .entry-content -->

</article><!-- #post-## -->
         
            
<article id="post-37" class="post-container post-37 post type-post status-publish format-standard has-post-thumbnail hentry category-marketing">
   <h2 class="entry-title"><a href="http://resources.pulsecomms.com/voodoo-account-based-marketing/" rel="bookmark">The voodoo of Account Based Marketing</a></h2>
         <div class="wider-web-top">
         <i class="fa fa-2x fa-caret-down"></i>
      </div>
      <figure>
         <a href="http://resources.pulsecomms.com/voodoo-account-based-marketing/">
            <img width="350" height="205" src="http://hubdo.relevante.me/wp-content/uploads/2017/01/Ricky-ABM-blog-696x500-350x205.png" class="attachment-small-thumb size-small-thumb wp-post-image" alt="" />         </a>
      </figure>
   
         <div class="entry-info">
         <div class="entry-date fa fa-clock-o"><a href="http://resources.pulsecomms.com/voodoo-account-based-marketing/" title="5:03 pm" rel="bookmark"><time class="entry-date published" datetime="2017-01-23T17:03:13+00:00">January 23, 2017</time><time class="updated" datetime="2017-01-23T17:13:22+00:00">January 23, 2017</time></a></div><div class="entry-author vcard author fa fa-user"><a class="url fn n" href="http://hubdo.relevante.me/author/relevante-me/">relevante.me</a></div><div class="entry-standard fa fa-folder-open"><a href="http://hubdo.relevante.me/category/marketing/" rel="category tag">Marketing</a></div>      </div><!-- .entry-meta -->
   
   <div class="entry-content">
      <p>Account based marketing is dividing marketers everywhere. It’s certainly a very fashionable term like ‘content marketing’ or ‘programmatic’ before it, but as always, it is very often misused. I’m sure there are marketers everywhere whose eyes roll when they hear</p>

            <a class="button" href="http://resources.pulsecomms.com/voodoo-account-based-marketing/">Read more</a>
   </div><!-- .entry-content -->

</article><!-- #post-## -->
         
      
      
<div class="secondary">
   <style type="text/css" id="wp_cta_css_custom_26_0" class="wp_cta_css_26 "> #wp_cta_26_variation_0 #cta_container #content {background: transparent;}
#wp_cta_26_variation_0 #cta_container {margin: auto;}
#wp_cta_26_variation_0 #cta_container #content {width: 400px;background: #222;padding-bottom: 15px;}
#wp_cta_26_variation_0 #cta_container p {padding-right: 0px;padding-left: 0px;text-align: center;color: #fff;}
#wp_cta_26_variation_0 #cta_container p:first-child {margin-top: 0px;padding-top: 0px;}
#wp_cta_26_variation_0 #cta_container p:last-child {margin-bottom: 0px;padding-bottom: 0px;}
#wp_cta_26_variation_0 #cta_container {text-align: center;font-family: Calibri,Helvetica,Arial,sans-serif;font-weight: 300;}
#wp_cta_26_variation_0 #cta_container .the_content {font-family: Calibri,Helvetica,Arial,sans-serif;padding-left: 10px;padding-right: 10px;padding: 10px;display: block;width: 80%;margin: auto;}
#wp_cta_26_variation_0 #cta_container #cta-link {text-decoration: none;}
#wp_cta_26_variation_0 #cta_container .button {display: block;cursor: pointer;width: 200px;font-size: 22px;margin: auto;margin-top: 15px;margin-bottom: 15px;height: 50px;line-height: 50px;text-transform: uppercase;background: #db3d3d;border-bottom: 3px solid #c12424;color: #fff;text-decoration: none;border-radius: 5px;transition: all .4s ease-in-out;}
#wp_cta_26_variation_0 #cta_container  .button:hover {background: #c12424;border-bottom: 3px solid #db3d3d;}
#wp_cta_26_variation_0 #cta_container  .clicked {transform: rotateY(-80deg);}
#wp_cta_26_variation_0 #cta_container {background-color: #222;padding-top: 28px;padding-bottom: 30px;padding-left: 20px;padding-right: 20px;color: #fff;text-align: center;}
#wp_cta_26_variation_0 .cta_content h1, #wp_cta_26_variation_0 .cta_content h2, #wp_cta_26_variation_0 .cta_content h3, #wp_cta_26_variation_0 .cta_content h4, #wp_cta_26_variation_0 .cta_content h5, #wp_cta_26_variation_0 .cta_content h6 {color: #fff;}
#wp_cta_26_variation_0 #cta_container #main-headline {color: #fff;}
#wp_cta_26_variation_0 .cta_content {padding-bottom: 5px;}
#wp_cta_26_variation_0 .cta_button, #wp_cta_26_variation_0 #cta_container input[type="button"], #wp_cta_26_variation_0 #cta_container button[type="submit"], #wp_cta_26_variation_0 #cta_container input[type="submit"] {text-align: center;background: #2cc9ad;border-bottom: 3px solid #28b69d;color: #fff;padding-left: 20px;padding-right: 20px;padding-top: 7px;padding-bottom: 7px;text-decoration: none;border-radius: 5px;transition: all .4s ease-in-out;margin-top: 10px;display: block;font-size: 1.3em;}
#wp_cta_26_variation_0 #cta_container form input[type="button"], #wp_cta_26_variation_0 #cta_container form button[type="submit"], #wp_cta_26_variation_0 #cta_container form input[type="submit"] {margin: auto;width: 91%;display: block;font-size: 1.3em;}
#wp_cta_26_variation_0 .cta_button:hover, #wp_cta_26_variation_0 #cta_container input[type="button"]:hover, #wp_cta_26_variation_0 #cta_container button[type="submit"]:hover, #wp_cta_26_variation_0 #cta_container input[type="submit"]:hover {background: #28b69d;border-bottom: 3px solid #2cc9ad;}
#wp_cta_26_variation_0 #cta_container  h1#main-headline {color: #fff;margin-top: 0px;padding-top: 10px;line-height: 36px;margin-bottom: 10px;font-weight: 300;font-size: 20px;padding-right: 0px;padding-left: 0px;}
#wp_cta_26_variation_0 #cta_container a {text-decoration: none;}
#wp_cta_26_variation_0 .cta_content input[type=text], #wp_cta_26_variation_0 .cta_content input[type=url], #wp_cta_26_variation_0 .cta_content input[type=email], #wp_cta_26_variation_0 .cta_content input[type=tel], #wp_cta_26_variation_0 .cta_content input[type=number], #wp_cta_26_variation_0 .cta_content input[type=password] {width: 90%;}
#wp_cta_26_variation_0 form {max-width: 330px;margin: auto;}</style><div id='wp_cta_26_container' class='wp_cta_container cta_outer_container' style='margin-top:px;margin-bottom:30px;position:relative;' ><div id='wp_cta_26_variation_0' class='inbound-cta-container wp_cta_content wp_cta_variation wp_cta_26_variation_0' style='display:none; margin:auto; width: px; height: px;'  data-variation='0' data-cta-id='26'><div id="cta_container">&#13;
  <h1 id="main-headline">Start saving time with relevante.me</h1>&#13;
&#13;
    <div class="cta_content">&#13;
	Personal but scalable lead generation. Build and warm up prospects lists with a cadence of social interactions.&#13;
    </div>&#13;
&#13;
		<a target="_blank" id="cta-link" href="http://hubdo.relevante.me/inbound/ei92473dn" rel="nofollow">&#13;
			<span class="cta_button">Click here</span>&#13;
		</a>&#13;
&#13;
</div></div></div>			<script>
			jQuery(document).ready(function($) {
				wp_cta_load_variation('26', null, '' );
			});
			</script>
					<aside id="recent-posts-2" class="blog-post widget widget_recent_entries">		<div class="widget-title"><h3>Recent Posts</h3></div>		<ul>
					<li>
				<a href="https://hub.uberflip.com/blog/how-b2b-brands-are-using-content-hubs">How B2B Brands Are Using Content Hubs</a>
						</li>
					<li>
				<a href="https://www.linkedin.com/pulse/linkedin-strategy-selling-high-value-products-lawrence-klamecki?trk=prof-post">LinkedIn Strategy: Selling High Value Products and Services</a>
						</li>
					<li>
				<a href="http://www.b2bleadblog.com/2016/12/what-can-b2b-marketers-adopt-from-growth-hacking.html">What Can B2B Marketers Gain from Growth Hacking?</a>
						</li>
					<li>
				<a href="http://resources.pulsecomms.com/voodoo-account-based-marketing/">The voodoo of Account Based Marketing</a>
						</li>
				</ul>
		</aside>		</div>
   </div><!-- #container -->
   <div class="wrapper">
         </div>
</div><!-- .site-content -->


</div><!-- #page -->
<footer class="footer-background">
   <div class="footer-content wrapper clear">
      <div class="clear">
                     <div class="tg-one-third">
               <aside id="recent-comments-3" class="widget widget_recent_comments"><div class="widget-title"><h3>Recent Comments</h3></div><ul id="recentcomments"></ul></aside>            </div>
                              <div class="tg-one-third">
               <aside id="categories-3" class="widget widget_categories"><div class="widget-title"><h3>Categories</h3></div>		<ul>
	<li class="cat-item cat-item-4"><a href="http://hubdo.relevante.me/category/b2b/" >B2B</a> (2)
</li>
	<li class="cat-item cat-item-5 current-cat"><a href="http://hubdo.relevante.me/category/marketing/" >Marketing</a> (2)
</li>
		</ul>
</aside>            </div>
                              <div class="tg-one-third last">
               <aside id="wysija-2" class="widget widget_wysija"><div class="widget-title"><h3>Get free updates from this HUB</h3></div><div class="widget_wysija_cont"><div id="msg-form-wysija-2" class="wysija-msg ajax"></div><form id="form-wysija-2" method="post" action="#wysija" class="widget_wysija">
<p class="wysija-paragraph">
    
    
    	<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="e-mail" placeholder="e-mail" value="" />
    
    
    
    <span class="abs-req">
        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
    </span>
    
</p>
<input class="wysija-submit wysija-submit-field" type="submit" value="Subscribe!" />

    <input type="hidden" name="form_id" value="1" />
    <input type="hidden" name="action" value="save" />
    <input type="hidden" name="controller" value="subscribers" />
    <input type="hidden" value="1" name="wysija-page" />

    
        <input type="hidden" name="wysija[user_list][list_ids]" value="1" />
    
 </form></div></aside>            </div>
               </div>
      <div class="copyright clear">
         <div class="copyright-header">HUB relevante.me</div>
         <div class="copyright-year">&copy; 2017</div>
         Powered by <a href="http://wordpress.org" target="_blank" title="WordPress"><span>WordPress</span></a> <br> Theme: Masonic by <a href="http://themegrill.com/themes/masonic" target="_blank" title="ThemeGrill" rel="author"><span>ThemeGrill</span></a>      </div>
   </div>
   <div class="angled-background"></div>
</footer>

<style scoped>.yuzo_related_post{}
.yuzo_related_post .relatedthumb{}</style><script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/cta/shared//shortcodes/js/spin.min.js'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/admin-bar.min.js?ver=4.7.1'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/yuzo-related-post/assets/js/jquery.equalizer.js?ver=5.12.68'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/imagesloaded.min.js?ver=3.2.0'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/masonry.min.js?ver=3.3.2'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/jquery/jquery.masonry.min.js?ver=3.1.2b'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/masonry-setting.js?ver=20150106'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/search-toggle.js?ver=20150106'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/fitvids/jquery.fitvids.js?ver=20150331'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/fitvids/fitvids-setting.js?ver=20150331'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/skip-link-focus-fix.js?ver=20130115'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/themes/masonic/js/jquery.bxslider/jquery.bxslider.min.js?ver=20130115'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/youtube-embed-plus/scripts/fitvids.min.js?ver=4.7.1'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-includes/js/wp-embed.min.js?ver=4.7.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var icl_vars = {"current_language":"en","icl_home":"http:\/\/hubdo.relevante.me","ajax_url":"http:\/\/hubdo.relevante.me\/wp-admin\/admin-ajax.php","url_type":"3"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/sitepress-multilingual-cms/res/js/sitepress.js?ver=4.7.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var call_out_box = {"ajaxurl":"http:\/\/hubdo.relevante.me\/wp-admin\/admin-ajax.php","post_id":"5"};
/* ]]> */
</script>
<script type='text/javascript' src='http://hub.relevante.me/wp-content/plugins/cta/templates/call-out-box/assets/js/modernizr.js'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-en.js?ver=2.7.6'></script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.6'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http:\/\/hubdo.relevante.me\/wp-admin\/admin-ajax.php","loadingTrans":"Loading...","is_rtl":""};
/* ]]> */
</script>
<script type='text/javascript' src='http://hubdo.relevante.me/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.6'></script>
<script>var pltNewTabURLs = ["https:\/\/www.linkedin.com\/pulse\/linkedin-strategy-selling-high-value-products-lawrence-klamecki?trk=prof-post","http:\/\/resources.pulsecomms.com\/voodoo-account-based-marketing\/","https:\/\/hub.uberflip.com\/blog\/how-b2b-brands-are-using-content-hubs","http:\/\/www.b2bleadblog.com\/2016\/12\/what-can-b2b-marketers-adopt-from-growth-hacking.html"];(function(){(function(e){var t;t=e.jQueryWP||e.jQuery;return t(function(e){return typeof e.fn.on=="function"?e("body").on("click","a",function(t){var n;n=e(this);if(e.inArray(n.attr("href"),pltNewTabURLs)>-1)return n.attr("target","_blank")}):typeof console!="undefined"&&console!==null?console.log("Page Links To: Some other code has overridden the WordPress copy of jQuery. This is bad. Because of this, Page Links To cannot open links in a new window."):void 0})})(window)}).call(this);</script>	<!--[if lte IE 8]>
		<script type="text/javascript">
			document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
		</script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
		<script type="text/javascript">
			(function() {
				var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

						request = true;
		
				b[c] = b[c].replace( rcs, ' ' );
				// The customizer requires postMessage and CORS (if the site is cross domain)
				b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
			}());
		</script>
	<!--<![endif]-->
			<div id="wpadminbar" class="nojq nojs">
							<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Skip to toolbar</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Toolbar" tabindex="0">
				<ul id="wp-admin-bar-root-default" class="ab-top-menu">
		<li id="wp-admin-bar-wp-logo" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://hubdo.relevante.me/wp-admin/about.php"><span class="ab-icon"></span><span class="screen-reader-text">About WordPress</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
		<li id="wp-admin-bar-about"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/about.php">About WordPress</a>		</li></ul><ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
		<li id="wp-admin-bar-wporg"><a class="ab-item" href="https://wordpress.org/">WordPress.org</a>		</li>
		<li id="wp-admin-bar-documentation"><a class="ab-item" href="https://codex.wordpress.org/">Documentation</a>		</li>
		<li id="wp-admin-bar-support-forums"><a class="ab-item" href="https://wordpress.org/support/">Support Forums</a>		</li>
		<li id="wp-admin-bar-feedback"><a class="ab-item" href="https://wordpress.org/support/forum/requests-and-feedback">Feedback</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-site-name" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://hubdo.relevante.me/wp-admin/">HUB relevante.me</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-site-name-default" class="ab-submenu">
		<li id="wp-admin-bar-dashboard"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/">Dashboard</a>		</li></ul><ul id="wp-admin-bar-appearance" class="ab-submenu">
		<li id="wp-admin-bar-themes"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/themes.php">Themes</a>		</li>
		<li id="wp-admin-bar-widgets"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/widgets.php">Widgets</a>		</li>
		<li id="wp-admin-bar-menus"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/nav-menus.php">Menus</a>		</li>
		<li id="wp-admin-bar-background" class="hide-if-customize"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/themes.php?page=custom-background">Background</a>		</li>
		<li id="wp-admin-bar-header" class="hide-if-customize"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/themes.php?page=custom-header">Header</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-customize" class="hide-if-no-customize"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/customize.php?url=http%3A%2F%2Fhubdo.relevante.me%2Fcategory%2Fmarketing%2F">Customize</a>		</li>
		<li id="wp-admin-bar-comments"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/edit-comments.php"><span class="ab-icon"></span><span class="ab-label awaiting-mod pending-count count-0" aria-hidden="true">0</span><span class="screen-reader-text">0 comments awaiting moderation</span></a>		</li>
		<li id="wp-admin-bar-new-content" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://hubdo.relevante.me/wp-admin/post-new.php"><span class="ab-icon"></span><span class="ab-label">New</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-new-content-default" class="ab-submenu">
		<li id="wp-admin-bar-new-post"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/post-new.php">Post</a>		</li>
		<li id="wp-admin-bar-new-media"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/media-new.php">Media</a>		</li>
		<li id="wp-admin-bar-new-page"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/post-new.php?post_type=page">Page</a>		</li>
		<li id="wp-admin-bar-new-wp-call-to-action"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/post-new.php?post_type=wp-call-to-action">Calls to Action</a>		</li>
		<li id="wp-admin-bar-new-inbound-forms"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/post-new.php?post_type=inbound-forms">Form</a>		</li>
		<li id="wp-admin-bar-new-user"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/user-new.php">User</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-edit"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/term.php?taxonomy=category&#038;tag_ID=5&#038;post_type=post">Edit Category</a>		</li>
		<li id="wp-admin-bar-inbound-admin-bar" class="menupop inbound-nav-marketing"><div class="ab-item ab-empty-item" aria-haspopup="true" title="Inbound Marketing Admin"> Marketing</div><div class="ab-sub-wrapper"><ul id="wp-admin-bar-inbound-admin-bar-default" class="ab-submenu">
		<li id="wp-admin-bar-inbound-cta" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://hubdo.relevante.me/wp-admin/edit.php?post_type=wp-call-to-action" title="View All Landing Pages">Call to Actions</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-inbound-cta-default" class="ab-submenu">
		<li id="wp-admin-bar-inbound-cta-view"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/post-new.php?post_type=wp-call-to-action" title="View All Landing Pages">View Calls to Action List</a>		</li>
		<li id="wp-admin-bar-inbound-cta-add"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/post-new.php?post_type=wp-call-to-action" title="Add new call to action">Add New Call to Action</a>		</li>
		<li id="wp-admin-bar-inbound-cta-categories"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/edit-tags.php?taxonomy=wp_call_to_action_category&#038;post_type=wp-call-to-action" title="Landing Page Categories">Categories</a>		</li>
		<li id="wp-admin-bar-inbound-cta-settings"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/edit.php?post_type=wp-call-to-action&#038;page=wp_cta_global_settings" title="Manage Call to Action Settings">Settings</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-inbound-forms" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://hubdo.relevante.me/wp-admin/edit.php?post_type=inbound-forms" title="Manage Forms">Manage Forms</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-inbound-forms-default" class="ab-submenu">
		<li id="wp-admin-bar-inbound-forms-view"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/edit.php?post_type=inbound-forms" title="View All Forms">View All Forms</a>		</li>
		<li id="wp-admin-bar-inbound-forms-add"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/post-new.php?post_type=inbound-forms" title="Add new call to action">Create New Form</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-inbound-settings" class="menupop"><div class="ab-item ab-empty-item" aria-haspopup="true" title="Manage Settings">Settings</div><div class="ab-sub-wrapper"><ul id="wp-admin-bar-inbound-settings-default" class="ab-submenu">
		<li id="wp-admin-bar-inbound-ctasettings"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/edit.php?post_type=wp-call-to-action&#038;page=wp_cta_global_settings" title="Call to Action Settings">Call to Action Settings</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-inbound-analytics"><a class="ab-item" href="#" title="Analytics (coming soon)">Analytics (coming soon)</a>		</li></ul><ul id="wp-admin-bar-inbound-secondary" class="ab-sub-secondary ab-submenu">
		<li id="wp-admin-bar-inbound-support" class="inbound-new-tab"><a class="ab-item" href="https://support.inboundnow.com/" target="_blank" title="Support Forum">Support Forum</a>		</li>
		<li id="wp-admin-bar-inbound-docs" class="inbound-new-tab"><a class="ab-item" href="http://docs.inboundnow.com/" target="_blank" title="Documentation">Documentation</a>		</li>
		<li id="wp-admin-bar-inbound-docs-searchform"><div class="ab-item ab-empty-item" title="Search Docs"><form method="get" id="inbound-menu-form" action="//www.inboundnow.com/support/search/?action=bbp-search-request" class=" " target="_blank">
			  <input id="search-inbound-menu" type="text" placeholder="Search Docs" onblur="this.value=(this.value=='') ? 'Search Docs' : this.value;" onfocus="this.value=(this.value=='Search Docs') ? '' : this.value;" value="Search Docs" name="bbp_search" value="Search Docs" class="text inbound-search-input" />
			  <input type="hidden" name="post_type[]" value="docs" />
			  <input type="hidden" name="post_type[]" value="page" /><input type="submit" value="GO" class="inbound-search-go"  /></form></div>		</li>
		<li id="wp-admin-bar-inbound-hq" class="menupop  inbound-new-tab"><a class="ab-item" aria-haspopup="true" href="https://www.inboundnow.com/" target="_blank" title="Inbound Now Plugin HQ">Inbound Now Plugin HQ</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-inbound-hq-default" class="ab-submenu">
		<li id="wp-admin-bar-inbound-sites-dev" class="inbound-new-tab"><a class="ab-item" href="https://github.com/inboundnow" target="_blank" title="GitHub Repository Developer Center">GitHub Repository Developer Center</a>		</li>
		<li id="wp-admin-bar-inbound-sites-blog" class="inbound-new-tab"><a class="ab-item" href="https://www.inboundnow.com/blog/" target="_blank" title="Official Blog">Official Blog</a>		</li>
		<li id="wp-admin-bar-inboundsitesaccount" class="menupop  inbound-new-tab"><a class="ab-item" aria-haspopup="true" href="https://www.inboundnow.com/marketplace/account/" target="_blank" title="My Account">My Account</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-inboundsitesaccount-default" class="ab-submenu">
		<li id="wp-admin-bar-inboundsitesaccount-history" class="inbound-new-tab"><a class="ab-item" href="https://www.inboundnow.com/marketplace/account/purchase-history/" target="_blank" title="Purchase History">Purchase History</a>		</li></ul></div>		</li></ul></div>		</li>
		<li id="wp-admin-bar-inbound-debug" class="menupop  inbound-new-tab"><a class="ab-item" aria-haspopup="true" href="#" target="_blank"><span style="color:#fff;font-size: 13px;margin-top: -1px;display: inline-block;">Debug Tools</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-inbound-debug-default" class="ab-submenu">
		<li id="wp-admin-bar-inbound-debug-checkjs" class="inbound-new-tab"><a class="ab-item" href="http://hubdo.relevante.me/category/marketing/?inbound_js" target="_blank" title="Click here to check javascript errors on this page">Check for Javascript Errors</a>		</li>
		<li id="wp-admin-bar-inbound-debug-turnoffscripts" class="inbound-new-tab"><a class="ab-item" href="http://hubdo.relevante.me/category/marketing/?inbound-dequeue-scripts" target="_blank" title="Click here to remove broken javascript to fix issues">Remove Javascript Errors</a>		</li></ul></div>		</li></ul></div>		</li>
		<li id="wp-admin-bar-itsec_admin_bar_menu" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://hubdo.relevante.me/wp-admin/admin.php?page=itsec">Security</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-itsec_admin_bar_menu-default" class="ab-submenu">
		<li id="wp-admin-bar-itsec_admin_bar_settings"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/admin.php?page=itsec">Settings</a>		</li>
		<li id="wp-admin-bar-itsec_admin_bar_security_check"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/admin.php?page=itsec&#038;module=security-check">Security Check</a>		</li>
		<li id="wp-admin-bar-itsec_admin_bar_logs"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/admin.php?page=itsec-logs">Logs</a>		</li></ul></div>		</li></ul><ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
		<li id="wp-admin-bar-search" class="admin-bar-search"><div class="ab-item ab-empty-item" tabindex="-1"><form action="http://hubdo.relevante.me/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Search</label><input type="submit" class="adminbar-button" value="Search"/></form></div>		</li>
		<li id="wp-admin-bar-my-account" class="menupop with-avatar"><a class="ab-item" aria-haspopup="true" href="http://hubdo.relevante.me/wp-admin/profile.php">Howdy, relevante.me<img alt='' src='http://0.gravatar.com/avatar/9ed284cb55d4a5c18fc7161635595839?s=26&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/9ed284cb55d4a5c18fc7161635595839?s=52&amp;d=mm&amp;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-user-actions" class="ab-submenu">
		<li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="http://hubdo.relevante.me/wp-admin/profile.php"><img alt='' src='http://0.gravatar.com/avatar/9ed284cb55d4a5c18fc7161635595839?s=64&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/9ed284cb55d4a5c18fc7161635595839?s=128&amp;d=mm&amp;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>relevante.me</span></a>		</li>
		<li id="wp-admin-bar-edit-profile"><a class="ab-item" href="http://hubdo.relevante.me/wp-admin/profile.php">Edit My Profile</a>		</li>
		<li id="wp-admin-bar-logout"><a class="ab-item" href="http://hubdo.relevante.me/wp-login.php?action=logout&#038;_wpnonce=9c8a30064a">Log Out</a>		</li></ul></div>		</li></ul>			</div>
						<a class="screen-reader-shortcut" href="http://hubdo.relevante.me/wp-login.php?action=logout&#038;_wpnonce=9c8a30064a">Log Out</a>
					</div>

		
</body>
</html>
<!-- Dynamic page generated in 0.624 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2017-01-23 18:39:00 -->
