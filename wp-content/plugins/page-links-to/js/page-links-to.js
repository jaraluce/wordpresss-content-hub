// Generated by CoffeeScript 1.7.1
(function() {
  jQuery(function($) {
    var input, section;
    section = $('#cws-links-to-custom-section');
    input = $('input[type=radio]', '#page-links-to');
    if (input.filter('input[value="wp"]').prop('checked')) {
      section.fadeTo(1, 0).hide();
    }
    return input.change(function() {
      if ($(this).val() === 'wp') {
        return section.fadeTo('fast', 0, function() {
          return $(this).slideUp();
        });
      } else {
        return section.slideDown('fast', function() {
          return $(this).fadeTo('fast', 1, function() {
            var i;
            i = $('#cws-links-to');
            return i.focus().val(i.val());
          });
        });
      }
    });
  });

}).call(this);
