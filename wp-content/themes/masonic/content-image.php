<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package ThemeGrill
 * @subpackage Masonic
 * @since 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post-container post-image-only'); ?>>

   <?php
      if( has_post_thumbnail() ) {
      ?>
      <figure>
         <a href="<?php echo get_the_content(); ?>" target="_blank">
            <?php
            the_post_thumbnail('big-thumb');
            ?>
         </a>
      </figure>
   <?php } ?>

</article><!-- #post-## -->