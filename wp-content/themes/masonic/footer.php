<?php
/**
 * Theme Footer Section for our theme.
 * 
 * Displays all of the footer section and closing of the #page div.
 *
 * @package ThemeGrill
 * @subpackage Masonic
 * @since 1.0
 */
?>

</div><!-- #page -->
<footer class="footer-background">
   <div class="footer-content wrapper clear">
      <div class="clear">
         <?php if (is_active_sidebar('footer-sidebar-one')) { ?>
            <div class="tg-one-third">
               <?php
               dynamic_sidebar('footer-sidebar-one');
               ?>
            </div>
         <?php } ?>
         <?php if (is_active_sidebar('footer-sidebar-two')) { ?>
            <div class="tg-one-third">
               <?php
               dynamic_sidebar('footer-sidebar-two');
               ?>
            </div>
         <?php } ?>
         <?php if (is_active_sidebar('footer-sidebar-three')) { ?>
            <div class="tg-one-third last">
               <?php
               dynamic_sidebar('footer-sidebar-three');
               ?>
            </div>
         <?php } ?>
      </div>
      <div class="copyright clear">
         <div class="copyright-header"><?php echo bloginfo('name'); ?></div>
         <div class="copyright-year"><?php
            _e('&copy; ', 'masonic');
            echo date('Y');
            ?></div>
         Powered by <a href="https://relevante.me" target="_blank">relevante.me</a>
      </div>
   </div>
   <div class="angled-background"></div>
</footer>
<?php wp_footer(); ?>

<script type="text/javascript" src="/wp-content/themes/masonic/js/script.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

<script type="text/javascript">
  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  var email;
  if (getParameterByName('email')) {
    email = getParameterByName('email');
  } else {
    email = null;
  }
  console.log(email)
  
  var referrer;
  if (jQuery.cookie('relevante_referrer') == null || jQuery.cookie('relevante_referrer') == "" ) {
      console.log('SET COOKIE')
      jQuery.cookie('relevante_referrer', document.referrer, { expires: 180 });
      referrer = document.referrer;
  } else {
    referrer = jQuery.cookie('relevante_referrer');
  }

  //Intercom
  Intercom('update',
    {
      email: email,
      "referrer_email" : email,
      "referrer_url" : referrer
    });
</script>

</body>
</html>