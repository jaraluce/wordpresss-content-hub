<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package ThemeGrill
 * @subpackage Masonic
 * @since 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post-container post-book-chapter'); ?>>
   <h2 class="entry-title"><a href="<?php echo get_the_content(); ?>" target="_blank" rel="bookmark"><?php the_title(); ?></a></h2>

   <?php
      if( has_post_thumbnail() ) {
      ?>
      <div class="wider-web-top">
         <i class="fa fa-2x fa-caret-down"></i>
      </div>
      <figure>
         <a href="<?php echo get_the_content(); ?>" target="_blank">
            <?php
            the_post_thumbnail('small-thumb');
            ?>
         </a>
      </figure>
   <?php } ?>

   <div class="entry-content">
      <a href="<?php echo get_the_content(); ?>" target="_blank" rel="bookmark">
      <?php
      the_excerpt();
      ?>
      </a>

      <?php
      wp_link_pages(array(
          'before' => '<div class="page-links">' . __('Pages:', 'masonic'),
          'after' => '</div>',
      ));
      ?>
   </div><!-- .entry-content -->

</article><!-- #post-## -->