<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/var/www/html/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'Q7wBriqrYS');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'T*)W&vF97pXV~}3S.y>I>xgIqxFvOE}IH3K0Do0}6GZN1m4QFKg?;P?sUazZ;D3.');
define('SECURE_AUTH_KEY',  '4WzNs;pn_)B#xYv9WC%P/j):Gsu=sDVW3dwYq.u4Xz^ {I2p~v3*H*}U8`U{R)_j');
define('LOGGED_IN_KEY',    'Q]Tya[IeDsfAjXLQx|%#rt#g)U-HU]AQe zeBfc^80f#(yK(4q@2Y%q[oO)Cz@xQ');
define('NONCE_KEY',        '/*r9-T*Z6PY,89J^qe):Y?CT{5Ih]iB $8Dj5N%u?dC#^2(AHge&1G~,cJ6,fRWW');
define('AUTH_SALT',        'iX~jMm&S,h);7RRcL{@!XStY_77ZN/UDPp<R@oMn$%h:=..WKjv|K!WFR^;=V<+-');
define('SECURE_AUTH_SALT', '@BRU9XM6V V1<AnQh7=(^i6W92zCG^cXiX*{.:dYmwFD-b27=`ClH#t_})TIO0/i');
define('LOGGED_IN_SALT',   'rEJ~]65uY#UGI>&c%l&TKQKrnMC_KJ-@1`dIqhQA%F=YWacU2/7IbmbOM}Cs%OwE');
define('NONCE_SALT',       '7)m/Bhi3EbS8bGEJPj bB]Wn7BR<%AXd1*j<m1P0vObd1H!lBo?~TTUjH*FcSFQg');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD', 'direct');
